package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    public List<FridgeItem> fridgeItem,
    items = new ArrayList<FridgeItem>();
    int maxSize = 20;

    @Override
    public int totalSize() {
        return maxSize;
    }
    @Override
    public int nItemsInFridge() {
        return items.size();
    }
    @Override
    public boolean placeIn(FridgeItem item) {
        if (items.size() >= totalSize())
            return false;
        return items.add(item);
    }
    @Override
    public void takeOut(FridgeItem item) {
        if (!items.contains(item))
            throw new NoSuchElementException("Item not in fridge");
        items.remove(item);
    }
    @Override
    public void emptyFridge() {
        items.clear();   
    }
    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList <FridgeItem> expiredItems = new ArrayList<>();
        for (int i = 0; i < nItemsInFridge(); i ++) {
            FridgeItem item = items.get(i);
            if (item.hasExpired()) {
                expiredItems.add(item);
            }
        }
        for (FridgeItem expItem : expiredItems) {
            items.remove(expItem);
        }
        return expiredItems;
    }
    
}
